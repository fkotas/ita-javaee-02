package cz.ita.javaee.repository.namedparamtemplate;

import java.util.Collections;
import java.util.List;

import cz.ita.javaee.model.Country;
import cz.ita.javaee.repository.api.CountryRepository;
import cz.ita.javaee.test.SpringContextTest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
@Rollback
@ActiveProfiles(value = "namedParameterJdbcTemplate", inheritProfiles = false)
public class CountryNamedParamTemplateRepositoryTest extends SpringContextTest {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    private CountryRepository countryRepository;

    @Test
    public void findOneTest() {
        Long firstId = jdbcTemplate.queryForObject("SELECT ID FROM COUNTRY ORDER BY ID LIMIT 1", Collections.EMPTY_MAP, Long.class);
        //first country should be Afganistan
        Country firstCountry = countryRepository.findOne(firstId);
        Assert.assertEquals("Afghanistan", firstCountry.getName());
        Assert.assertEquals("AFG", firstCountry.getIsoCode());
    }

    @Test
    public void findTest() {
        List<Country> countries = countryRepository.find(0, 10, "ID");
        Assert.assertThat(countries, Matchers.hasSize(5));
    }

    @Test
    public void findOffsetTest() {
        List<Country> countries = countryRepository.find(4, 10, "ID");
        Assert.assertThat(countries, Matchers.hasSize(1));
    }

    @Test
    public void findLimitTest() {
        List<Country> countries = countryRepository.find(1, 1, "ID");
        Assert.assertThat(countries, Matchers.hasSize(1));
    }

    @Test
    public void deleteTest() {
        jdbcTemplate.update("DELETE FROM CONTACT_GROUP_CONTACT", Collections.EMPTY_MAP);
        jdbcTemplate.update("DELETE FROM CONTACT_GROUP", Collections.EMPTY_MAP);
        jdbcTemplate.update("DELETE FROM CONTACT", Collections.EMPTY_MAP);
        jdbcTemplate.update("DELETE FROM ADDRESS", Collections.EMPTY_MAP);

        Long firstId = jdbcTemplate.queryForObject("SELECT ID FROM COUNTRY ORDER BY ID LIMIT 1", Collections.EMPTY_MAP, Long.class);
        int deletedCount = countryRepository.delete(firstId);

        Assert.assertEquals(1, deletedCount);

        Integer countryCount = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM COUNTRY", Collections.EMPTY_MAP, Integer.class);
        Assert.assertEquals((Integer) 4, countryCount);
    }

    @Test
    public void updateTest() {
        Long firstId = jdbcTemplate.queryForObject("SELECT ID FROM COUNTRY ORDER BY ID LIMIT 1", Collections.EMPTY_MAP, Long.class);

        Country afganistanCountry = countryRepository.findOne(firstId);

        String newName = "NewName";
        String newIsoCode = "NIC";
        afganistanCountry.setName(newName);
        afganistanCountry.setIsoCode(newIsoCode);

        Country updatedCountry = countryRepository.update(afganistanCountry);

        Assert.assertEquals(newName, updatedCountry.getName());
        Assert.assertEquals(newName, jdbcTemplate.queryForObject("SELECT NAME FROM COUNTRY WHERE ID=:id", new MapSqlParameterSource("id", firstId), String.class));
        Assert.assertEquals(newIsoCode, updatedCountry.getIsoCode());
        Assert.assertEquals(newIsoCode, jdbcTemplate.queryForObject("SELECT ISO_CODE FROM COUNTRY WHERE ID=:id", new MapSqlParameterSource("id", firstId), String.class));
    }

    @Test
    public void createTest() {
        Country country = new Country();
        String newName = "NewName";
        String newIsoCode = "NIC";
        country.setName(newName);
        country.setIsoCode(newIsoCode);

        Country newCountry = countryRepository.create(country);

        Long lastId = jdbcTemplate.queryForObject("SELECT ID FROM COUNTRY ORDER BY ID DESC LIMIT 1", EmptySqlParameterSource.INSTANCE, Long.class);

        Assert.assertEquals(lastId, newCountry.getId());
        Assert.assertEquals(newName, newCountry.getName());
        MapSqlParameterSource paramSource = new MapSqlParameterSource("id", lastId);
        Assert.assertEquals(newName, jdbcTemplate.queryForObject("SELECT NAME FROM COUNTRY WHERE ID=:id",
                paramSource, String.class));
        Assert.assertEquals(newIsoCode, newCountry.getIsoCode());
        Assert.assertEquals(newIsoCode, jdbcTemplate.queryForObject("SELECT ISO_CODE FROM COUNTRY WHERE ID=:id",
                paramSource, String.class));
    }
}
