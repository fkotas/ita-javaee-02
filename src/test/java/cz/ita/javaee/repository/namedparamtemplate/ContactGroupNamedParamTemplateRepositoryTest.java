package cz.ita.javaee.repository.namedparamtemplate;

import java.util.List;

import com.google.common.collect.Sets;
import cz.ita.javaee.model.Address;
import cz.ita.javaee.model.ContactGroup;
import cz.ita.javaee.model.PersonContact;
import cz.ita.javaee.repository.api.AddressRepository;
import cz.ita.javaee.repository.api.ContactGroupRepository;
import cz.ita.javaee.repository.api.PersonContactRepository;
import cz.ita.javaee.test.SpringContextTest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
@Rollback
@ActiveProfiles(value = "namedParameterJdbcTemplate", inheritProfiles = false)
public class ContactGroupNamedParamTemplateRepositoryTest extends SpringContextTest {

    private static final int FIRST_ID = 1;
    private static final int CONTACT_GROUP_CONTACT_COUNT = 2;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    private ContactGroupRepository contactGroupRepository;
    @Autowired
    private PersonContactRepository personContactRepository;
    @Autowired
    private AddressRepository addressRepository;

    @Test
    public void findOneTest() {
//        INSERT INTO CONTACT_GROUP(ID, NAME) VALUES(1, 'PERSONS');
        SqlParameterSource namedParameters = new MapSqlParameterSource("id", FIRST_ID);
        Long firstId = jdbcTemplate.queryForObject("SELECT ID FROM CONTACT_GROUP WHERE ID=:id", namedParameters, Long.class);
        ContactGroup personsContactGroup = contactGroupRepository.findOne(firstId);
        Assert.assertEquals("PERSONS", personsContactGroup.getName());
    }

    @Test
    public void findTest() {
        List<ContactGroup> contactGroups = contactGroupRepository.find(0, 10, "ID");
        Assert.assertThat(contactGroups, Matchers.hasSize(CONTACT_GROUP_CONTACT_COUNT));
    }

    @Test
    public void findOffsetTest() {
        List<ContactGroup> contactGroups = contactGroupRepository.find(1, 10, "ID");
        Assert.assertThat(contactGroups, Matchers.hasSize(CONTACT_GROUP_CONTACT_COUNT - 1));
    }

    @Test
    public void findLimitTest() {
        List<ContactGroup> contactGroups = contactGroupRepository.find(0, 1, "ID");
        Assert.assertThat(contactGroups, Matchers.hasSize(1));
    }

    @Test
    public void deleteTest() {
        int deletedCount = contactGroupRepository.delete(FIRST_ID);

        Assert.assertEquals(1, deletedCount);

        Integer contactGroupCount = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM CONTACT_GROUP", EmptySqlParameterSource.INSTANCE, Integer.class);
        Assert.assertEquals(CONTACT_GROUP_CONTACT_COUNT - 1, (int) contactGroupCount);
    }

    @Test
    public void updateTest() {
        ContactGroup contactGroup = contactGroupRepository.findOne(FIRST_ID);

        String newName = "NewName";
        contactGroup.setName(newName);

        Address address = addressRepository.findOne(FIRST_ID);
        PersonContact personContact = personContactRepository.create(new PersonContact("Roman", "Kudovic", "+420098098098", "rkudovic@ggmail.com", address));
        contactGroup.setContacts(Sets.newHashSet(personContact));
        contactGroupRepository.update(contactGroup);

        Assert.assertEquals(newName, contactGroup.getName());
        SqlParameterSource namedParameters = new MapSqlParameterSource("id", FIRST_ID);

        Assert.assertEquals(newName, jdbcTemplate.queryForObject("SELECT NAME FROM CONTACT_GROUP WHERE ID=:id", namedParameters, String.class));
        Assert.assertEquals((Long) 1L, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM CONTACT_GROUP_CONTACT WHERE CONTACT_GROUP_ID=:id", namedParameters, Long.class));
        Assert.assertEquals(personContact.getId(), jdbcTemplate.queryForObject("SELECT CONTACT_ID FROM CONTACT_GROUP_CONTACT WHERE CONTACT_GROUP_ID=:id", namedParameters, Long.class));
    }

    @Test
    public void createTest() {
        ContactGroup contactGroup = new ContactGroup();

        String newName = "NewName";
        contactGroup.setName(newName);

        Address address = addressRepository.findOne(FIRST_ID);
        PersonContact personContact = personContactRepository.create(new PersonContact("Roman", "Kudovic", "+420098098098", "rkudovic@ggmail.com", address));
        contactGroup.setContacts(Sets.newHashSet(personContact));

        ContactGroup createdContactGroup = contactGroupRepository.create(contactGroup);

        Long lastId = jdbcTemplate.queryForObject("SELECT ID FROM CONTACT_GROUP ORDER BY ID DESC LIMIT 1", EmptySqlParameterSource.INSTANCE, Long.class);

        Assert.assertEquals(lastId, createdContactGroup.getId());
        Assert.assertEquals(newName, contactGroup.getName());

        SqlParameterSource namedParameters = new MapSqlParameterSource("id", lastId);

        Assert.assertEquals(newName, jdbcTemplate.queryForObject("SELECT NAME FROM CONTACT_GROUP WHERE ID=:id", namedParameters, String.class));
        Assert.assertEquals((Long) 1L, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM CONTACT_GROUP_CONTACT WHERE CONTACT_GROUP_ID=:id", namedParameters, Long.class));
        Assert.assertEquals(personContact.getId(), jdbcTemplate.queryForObject("SELECT CONTACT_ID FROM CONTACT_GROUP_CONTACT WHERE CONTACT_GROUP_ID=:id", namedParameters, Long.class));
    }
}
