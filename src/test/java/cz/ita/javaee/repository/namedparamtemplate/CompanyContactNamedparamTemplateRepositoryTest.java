package cz.ita.javaee.repository.namedparamtemplate;

import java.util.List;

import cz.ita.javaee.model.Address;
import cz.ita.javaee.model.CompanyContact;
import cz.ita.javaee.repository.api.AddressRepository;
import cz.ita.javaee.repository.api.CompanyContactRepository;
import cz.ita.javaee.repository.api.CountryRepository;
import cz.ita.javaee.test.SpringContextTest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static cz.ita.javaee.model.Contact.ContactType.COMPANY;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
@Rollback
@ActiveProfiles(value = "namedParameterJdbcTemplate", inheritProfiles = false)
public class CompanyContactNamedparamTemplateRepositoryTest extends SpringContextTest {

    private static final int FIRST_ID = 3;
    private static final int COMPANY_CONTACT_COUNT = 2;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    private CompanyContactRepository companyContactRepository;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private CountryRepository countryRepository;

    @Test
    public void findOneTest() {
        //INSERT INTO CONTACT(ID, TYPE, FIRSTNAME, SURNAME, COMPANY_NAME, COMPANY_ID, VAT_ID, PHONE_NUMBER, EMAIL, ADDRESS_ID)
        // VALUES(3, 'COMPANY', NULL, NULL, 'AFG Mail', '999887', 'AF999887', '+660987643234', 'info22@afgmail.com', 3);
        CompanyContact firstCompanyContact = companyContactRepository.findOne(FIRST_ID);
        Assert.assertEquals("AFG Mail", firstCompanyContact.getCompanyName());
        Assert.assertEquals("999887", firstCompanyContact.getCompanyId());
        Assert.assertEquals("AF999887", firstCompanyContact.getVatId());
        Assert.assertEquals("+660987643234", firstCompanyContact.getPhoneNumber());
        Assert.assertEquals("info22@afgmail.com", firstCompanyContact.getEmail());
    }

    @Test
    public void findTest() {
        List<CompanyContact> countries = companyContactRepository.find(0, 10, "ID");
        Assert.assertThat(countries, Matchers.hasSize(COMPANY_CONTACT_COUNT));
    }

    @Test
    public void findOffsetTest() {
        List<CompanyContact> countries = companyContactRepository.find(1, 10, "ID");
        Assert.assertThat(countries, Matchers.hasSize(COMPANY_CONTACT_COUNT - 1));
    }

    @Test
    public void findLimitTest() {
        List<CompanyContact> countries = companyContactRepository.find(0, 1, "ID");
        Assert.assertThat(countries, Matchers.hasSize(1));
    }

    @Test
    public void deleteTest() {
        jdbcTemplate.update("DELETE FROM CONTACT_GROUP_CONTACT", EmptySqlParameterSource.INSTANCE);
        jdbcTemplate.update("DELETE FROM CONTACT_GROUP", EmptySqlParameterSource.INSTANCE);

        int deletedCount = companyContactRepository.delete(FIRST_ID);

        Assert.assertEquals(1, deletedCount);

        MapSqlParameterSource paramSource = new MapSqlParameterSource("type", COMPANY.name());
        Integer countryCount = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM CONTACT WHERE TYPE=:type",
                paramSource, Integer.class);
        Assert.assertEquals(COMPANY_CONTACT_COUNT - 1, (int) countryCount);
    }

    @Test
    public void updateTest() {

        CompanyContact companyContact = companyContactRepository.findOne(FIRST_ID);

        String newCompanyName = "NewCompanyName";
        String newCompanyId = "123456";
        String newVatId = "NewVatId";
        String newPhoneNumber = "NewPhoneNumber";
        String newEmail = "NewEmail";
        Address newAddress = new Address();
        newAddress.setStreet("Street A");
        newAddress.setStreetNumber("1/2");
        newAddress.setCity("Jihlava");
        newAddress.setZipCode("90876");
        newAddress.setCountry(countryRepository.findOne(FIRST_ID));
        addressRepository.create(newAddress);

        companyContact.setCompanyName(newCompanyName);
        companyContact.setCompanyId(newCompanyId);
        companyContact.setVatId(newVatId);
        companyContact.setPhoneNumber(newPhoneNumber);
        companyContact.setEmail(newEmail);
        companyContact.setAddress(newAddress);

        CompanyContact updatedContact = companyContactRepository.update(companyContact);

        Assert.assertEquals(newCompanyName, updatedContact.getCompanyName());
        Assert.assertEquals(newCompanyId, updatedContact.getCompanyId());
        Assert.assertEquals(newVatId, updatedContact.getVatId());
        Assert.assertEquals(newPhoneNumber, updatedContact.getPhoneNumber());
        Assert.assertEquals(newEmail, updatedContact.getEmail());
        Assert.assertEquals(newAddress.getId(), updatedContact.getAddress().getId());

        MapSqlParameterSource paramSource = new MapSqlParameterSource("id", FIRST_ID);

        Assert.assertEquals(newCompanyName, jdbcTemplate.queryForObject("SELECT COMPANY_NAME FROM CONTACT WHERE ID=:id", paramSource, String.class));
        Assert.assertEquals(newCompanyId, jdbcTemplate.queryForObject("SELECT COMPANY_ID FROM CONTACT WHERE ID=:id", paramSource, String.class));
        Assert.assertEquals(newVatId, jdbcTemplate.queryForObject("SELECT VAT_ID FROM CONTACT WHERE ID=:id", paramSource, String.class));
        Assert.assertEquals(newPhoneNumber, jdbcTemplate.queryForObject("SELECT PHONE_NUMBER FROM CONTACT WHERE ID=:id", paramSource, String.class));
        Assert.assertEquals(newEmail, jdbcTemplate.queryForObject("SELECT EMAIL FROM CONTACT WHERE ID=:id", paramSource, String.class));
        Assert.assertEquals(newAddress.getId(), jdbcTemplate.queryForObject("SELECT ADDRESS_ID FROM CONTACT WHERE ID=:id", paramSource, Long.class));
    }

    @Test
    public void createTest() {
        CompanyContact companyContact = new CompanyContact();

        String newCompanyName = "NewCompanyName";
        String newCompanyId = "123456";
        String newVatId = "NewVatId";
        String newPhoneNumber = "NewPhoneNumber";
        String newEmail = "NewEmail";

        Address newAddress = new Address();
        newAddress.setStreet("Street A");
        newAddress.setStreetNumber("1/2");
        newAddress.setCity("Jihlava");
        newAddress.setZipCode("90876");
        newAddress.setCountry(countryRepository.findOne(FIRST_ID));
        addressRepository.create(newAddress);

        companyContact.setCompanyName(newCompanyName);
        companyContact.setCompanyId(newCompanyId);
        companyContact.setVatId(newVatId);
        companyContact.setPhoneNumber(newPhoneNumber);
        companyContact.setEmail(newEmail);
        companyContact.setAddress(newAddress);

        CompanyContact createdContact = companyContactRepository.create(companyContact);

        Long lastId = jdbcTemplate.queryForObject("SELECT ID FROM CONTACT ORDER BY ID DESC LIMIT 1", EmptySqlParameterSource.INSTANCE, Long.class);

        Assert.assertEquals(lastId, createdContact.getId());
        Assert.assertEquals(newCompanyName, createdContact.getCompanyName());
        Assert.assertEquals(newCompanyId, createdContact.getCompanyId());
        Assert.assertEquals(newVatId, createdContact.getVatId());
        Assert.assertEquals(newPhoneNumber, createdContact.getPhoneNumber());
        Assert.assertEquals(newEmail, createdContact.getEmail());
        Assert.assertEquals(newAddress.getId(), createdContact.getAddress().getId());

        MapSqlParameterSource paramSource = new MapSqlParameterSource("id", lastId);

        Assert.assertEquals(newCompanyName, jdbcTemplate.queryForObject("SELECT COMPANY_NAME FROM CONTACT WHERE ID=:id", paramSource, String.class));
        Assert.assertEquals(newCompanyId, jdbcTemplate.queryForObject("SELECT COMPANY_ID FROM CONTACT WHERE ID=:id", paramSource, String.class));
        Assert.assertEquals(newVatId, jdbcTemplate.queryForObject("SELECT VAT_ID FROM CONTACT WHERE ID=:id", paramSource, String.class));
        Assert.assertEquals(newPhoneNumber, jdbcTemplate.queryForObject("SELECT PHONE_NUMBER FROM CONTACT WHERE ID=:id", paramSource, String.class));
        Assert.assertEquals(newEmail, jdbcTemplate.queryForObject("SELECT EMAIL FROM CONTACT WHERE ID=:id", paramSource, String.class));
        Assert.assertEquals(newAddress.getId(), jdbcTemplate.queryForObject("SELECT ADDRESS_ID FROM CONTACT WHERE ID=:id", paramSource, Long.class));
    }
}
