package cz.ita.javaee.repository.jdbctemplate;

import java.util.List;

import com.google.common.collect.Sets;
import cz.ita.javaee.model.Address;
import cz.ita.javaee.model.ContactGroup;
import cz.ita.javaee.model.PersonContact;
import cz.ita.javaee.repository.api.AddressRepository;
import cz.ita.javaee.repository.api.ContactGroupRepository;
import cz.ita.javaee.repository.api.PersonContactRepository;
import cz.ita.javaee.test.SpringContextTest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
@Rollback
@ActiveProfiles("jdbcTemplate")
//@ActiveProfiles("namedParameterJdbcTemplate")
public class ContactGroupJdbcTemplateRepositoryTest extends SpringContextTest {

    private static final int FIRST_ID = 1;
    private static final int CONTACT_GROUP_CONTACT_COUNT = 2;

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private ContactGroupRepository contactGroupRepository;
    @Autowired
    private PersonContactRepository personContactRepository;
    @Autowired
    private AddressRepository addressRepository;

    @Test
    public void findOneTest() {
//        INSERT INTO CONTACT_GROUP(ID, NAME) VALUES(1, 'PERSONS');
        Long firstId = jdbcTemplate.queryForObject("SELECT ID FROM CONTACT_GROUP WHERE ID=?", Long.class, FIRST_ID);
        ContactGroup personsContactGroup = contactGroupRepository.findOne(firstId);
        Assert.assertEquals("PERSONS", personsContactGroup.getName());
    }

    @Test
    public void findTest() {
        List<ContactGroup> contactGroups = contactGroupRepository.find(0, 10, "ID");
        Assert.assertThat(contactGroups, Matchers.hasSize(CONTACT_GROUP_CONTACT_COUNT));
    }

    @Test
    public void findOffsetTest() {
        List<ContactGroup> contactGroups = contactGroupRepository.find(1, 10, "ID");
        Assert.assertThat(contactGroups, Matchers.hasSize(CONTACT_GROUP_CONTACT_COUNT - 1));
    }

    @Test
    public void findLimitTest() {
        List<ContactGroup> contactGroups = contactGroupRepository.find(0, 1, "ID");
        Assert.assertThat(contactGroups, Matchers.hasSize(1));
    }

    @Test
    public void deleteTest() {
        int deletedCount = contactGroupRepository.delete(FIRST_ID);

        Assert.assertEquals(1, deletedCount);

        Integer contactGroupCount = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM CONTACT_GROUP", Integer.class);
        Assert.assertEquals(CONTACT_GROUP_CONTACT_COUNT - 1, (int) contactGroupCount);
    }

    @Test
    public void updateTest() {
        ContactGroup contactGroup = contactGroupRepository.findOne(FIRST_ID);

        String newName = "NewName";
        contactGroup.setName(newName);

        Address address = addressRepository.findOne(FIRST_ID);
        PersonContact personContact = personContactRepository.create(new PersonContact("Roman", "Kudovic", "+420098098098", "rkudovic@ggmail.com", address));
        contactGroup.setContacts(Sets.newHashSet(personContact));
        contactGroupRepository.update(contactGroup);

        Assert.assertEquals(newName, contactGroup.getName());
        Assert.assertEquals(newName, jdbcTemplate.queryForObject("SELECT NAME FROM CONTACT_GROUP WHERE ID=?", String.class, FIRST_ID));
        Assert.assertEquals((Long) 1L, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM CONTACT_GROUP_CONTACT WHERE CONTACT_GROUP_ID=?", Long.class, FIRST_ID));
        Assert.assertEquals(personContact.getId(), jdbcTemplate.queryForObject("SELECT CONTACT_ID FROM CONTACT_GROUP_CONTACT WHERE CONTACT_GROUP_ID=?", Long.class, FIRST_ID));
    }

    @Test
    public void createTest() {
        ContactGroup contactGroup = new ContactGroup();

        String newName = "NewName";
        contactGroup.setName(newName);

        Address address = addressRepository.findOne(FIRST_ID);
        PersonContact personContact = personContactRepository.create(new PersonContact("Roman", "Kudovic", "+420098098098", "rkudovic@ggmail.com", address));
        contactGroup.setContacts(Sets.newHashSet(personContact));

        ContactGroup createdContactGroup = contactGroupRepository.create(contactGroup);

        Long lastId = jdbcTemplate.queryForObject("SELECT ID FROM CONTACT_GROUP ORDER BY ID DESC LIMIT 1", Long.class);

        Assert.assertEquals(lastId, createdContactGroup.getId());
        Assert.assertEquals(newName, contactGroup.getName());

        Assert.assertEquals(newName, jdbcTemplate.queryForObject("SELECT NAME FROM CONTACT_GROUP WHERE ID=?", String.class, lastId));
        Assert.assertEquals((Long) 1L, jdbcTemplate.queryForObject("SELECT COUNT(*) FROM CONTACT_GROUP_CONTACT WHERE CONTACT_GROUP_ID=?", Long.class, lastId));
        Assert.assertEquals(personContact.getId(), jdbcTemplate.queryForObject("SELECT CONTACT_ID FROM CONTACT_GROUP_CONTACT WHERE CONTACT_GROUP_ID=?", Long.class, lastId));
    }
}
