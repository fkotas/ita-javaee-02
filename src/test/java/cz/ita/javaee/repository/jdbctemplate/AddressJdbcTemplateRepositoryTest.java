package cz.ita.javaee.repository.jdbctemplate;

import java.util.List;

import cz.ita.javaee.model.Address;
import cz.ita.javaee.model.Country;
import cz.ita.javaee.repository.api.AddressRepository;
import cz.ita.javaee.repository.api.CountryRepository;
import cz.ita.javaee.test.SpringContextTest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
@Rollback
@ActiveProfiles("jdbcTemplate")
//@ActiveProfiles("namedParameterJdbcTemplate")
public class AddressJdbcTemplateRepositoryTest extends SpringContextTest {

    private static final int ADDRESS_COUNT = 4;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private CountryRepository countryRepository;

    @Test
    public void findOneTest() {
        Long firstId = jdbcTemplate.queryForObject("SELECT ID FROM ADDRESS ORDER BY ID LIMIT 1", Long.class);
        //first address is from INSERT INTO ADDRESS(ID, STREET, STREET_NUMBER, CITY, ZIP_CODE, COUNTRY_ID) VALUES(1, 'Ceska', '7', 'Brno', '60200', 3);
        //INSERT INTO COUNTRY(ID, NAME, ISO_CODE) VALUES(3, 'Czech republic', 'CZE');
        Address address = addressRepository.findOne(firstId);
        Assert.assertEquals("Ceska", address.getStreet());
        Assert.assertEquals("7", address.getStreetNumber());
        Assert.assertEquals("Brno", address.getCity());
        Assert.assertEquals("60200", address.getZipCode());
        Assert.assertNotNull(address.getCountry());
        Assert.assertEquals((Long) 3L, address.getCountry().getId());
        Assert.assertEquals("Czech republic", address.getCountry().getName());
        Assert.assertEquals("CZE", address.getCountry().getIsoCode());
    }

    @Test
    public void findTest() {
        List<Address> addresses = addressRepository.find(0, ADDRESS_COUNT + 1, "ID");
        Assert.assertThat(addresses, Matchers.hasSize(ADDRESS_COUNT));
    }

    @Test
    public void findOffsetTest() {
        List<Address> addresses = addressRepository.find(1, ADDRESS_COUNT + 1, "ID");
        Assert.assertThat(addresses, Matchers.hasSize(ADDRESS_COUNT - 1));
    }

    @Test
    public void findLimitTest() {
        List<Address> addresses = addressRepository.find(1, 1, "ID");
        Assert.assertThat(addresses, Matchers.hasSize(1));
    }

    @Test
    public void deleteTest() {
        jdbcTemplate.update("DELETE FROM CONTACT_GROUP_CONTACT");
        jdbcTemplate.update("DELETE FROM CONTACT_GROUP");
        jdbcTemplate.update("DELETE FROM CONTACT");

        Long firstId = jdbcTemplate.queryForObject("SELECT ID FROM ADDRESS ORDER BY ID LIMIT 1", Long.class);
        int deletedCount = addressRepository.delete(firstId);

        Assert.assertEquals(1, deletedCount);

        Integer addressCount = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM ADDRESS", Integer.class);
        Assert.assertEquals(ADDRESS_COUNT - 1, (int) addressCount);
    }

    @Test
    public void updateTest() {
        Long firstId = jdbcTemplate.queryForObject("SELECT ID FROM ADDRESS ORDER BY ID LIMIT 1", Long.class);

        Country newCountry = countryRepository.findOne(1);

        Address firstAddress = addressRepository.findOne(firstId);
        String newStreet = "NewStreet";
        String newStreetNumber = "10";
        String newCity = "NewCity";
        String newZipCode = "00000";
        firstAddress.setStreet(newStreet);
        firstAddress.setStreetNumber(newStreetNumber);
        firstAddress.setCity(newCity);
        firstAddress.setZipCode(newZipCode);
        firstAddress.setCountry(newCountry);

        Address updatedAddress = addressRepository.update(firstAddress);

        Assert.assertEquals(newStreet, updatedAddress.getStreet());
        Assert.assertEquals(newStreetNumber, updatedAddress.getStreetNumber());
        Assert.assertEquals(newCity, updatedAddress.getCity());
        Assert.assertEquals(newZipCode, updatedAddress.getZipCode());

        Assert.assertEquals(newStreet, jdbcTemplate.queryForObject("SELECT STREET FROM ADDRESS WHERE ID=?", String.class, firstId));
        Assert.assertEquals(newStreetNumber, jdbcTemplate.queryForObject("SELECT STREET_NUMBER FROM ADDRESS WHERE ID=?", String.class, firstId));
        Assert.assertEquals(newCity, jdbcTemplate.queryForObject("SELECT CITY FROM ADDRESS WHERE ID=?", String.class, firstId));
        Assert.assertEquals(newZipCode, jdbcTemplate.queryForObject("SELECT ZIP_CODE FROM ADDRESS WHERE ID=?", String.class, firstId));
        Assert.assertEquals(newCountry.getId(), jdbcTemplate.queryForObject("SELECT COUNTRY_ID FROM ADDRESS WHERE ID=?", Long.class, firstId));
    }

    @Test
    public void createTest() {
        Country newCountry = countryRepository.findOne(1);

        Address address = new Address();
        String newStreet = "NewStreet";
        String newStreetNumber = "10";
        String newCity = "NewCity";
        String newZipCode = "00000";
        address.setStreet(newStreet);
        address.setStreetNumber(newStreetNumber);
        address.setCity(newCity);
        address.setZipCode(newZipCode);
        address.setCountry(newCountry);


        Address newAddress = addressRepository.create(address);

        Long lastId = jdbcTemplate.queryForObject("SELECT ID FROM ADDRESS ORDER BY ID DESC LIMIT 1", Long.class);

        Assert.assertEquals(lastId, newAddress.getId());
        Assert.assertEquals(newStreet, newAddress.getStreet());
        Assert.assertEquals(newStreetNumber, newAddress.getStreetNumber());
        Assert.assertEquals(newCity, newAddress.getCity());
        Assert.assertEquals(newZipCode, newAddress.getZipCode());
        Assert.assertEquals(newCountry, newAddress.getCountry());


        Assert.assertEquals(newStreet, jdbcTemplate.queryForObject("SELECT STREET FROM ADDRESS WHERE ID=?", String.class, lastId));
        Assert.assertEquals(newStreetNumber, jdbcTemplate.queryForObject("SELECT STREET_NUMBER FROM ADDRESS WHERE ID=?", String.class, lastId));
        Assert.assertEquals(newCity, jdbcTemplate.queryForObject("SELECT CITY FROM ADDRESS WHERE ID=?", String.class, lastId));
        Assert.assertEquals(newZipCode, jdbcTemplate.queryForObject("SELECT zip_code FROM ADDRESS WHERE ID=?", String.class, lastId));
        Assert.assertEquals(newCountry.getId(), jdbcTemplate.queryForObject("SELECT COUNTRY_ID FROM ADDRESS WHERE ID=?", Long.class, lastId));
    }
}
