package cz.ita.javaee.repository.jdbctemplate;

import java.util.List;

import cz.ita.javaee.model.Country;
import cz.ita.javaee.repository.api.CountryRepository;
import cz.ita.javaee.test.SpringContextTest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
@Rollback
@ActiveProfiles("jdbcTemplate")
//@ActiveProfiles("namedParameterJdbcTemplate")
public class CountryJdbcTemplateRepositoryTest extends SpringContextTest {
    private static final Long FIRST_ID = 1L;

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private CountryRepository countryRepository;

    @Test
    public void findOneTest() {
        //first country should be Afganistan
        Country firstCountry = countryRepository.findOne(FIRST_ID);
        Assert.assertEquals("Afghanistan", firstCountry.getName());
        Assert.assertEquals("AFG", firstCountry.getIsoCode());
    }

    @Test
    public void findTest() {
        List<Country> countries = countryRepository.find(0, 10, "ID");
        Assert.assertThat(countries, Matchers.hasSize(5));
    }

    @Test
    public void findOffsetTest() {
        List<Country> countries = countryRepository.find(4, 10, "ID");
        Assert.assertThat(countries, Matchers.hasSize(1));
    }

    @Test
    public void findLimitTest() {
        List<Country> countries = countryRepository.find(1, 1, "ID");
        Assert.assertThat(countries, Matchers.hasSize(1));
    }

    @Test
    public void deleteTest() {
        jdbcTemplate.update("DELETE FROM CONTACT_GROUP_CONTACT");
        jdbcTemplate.update("DELETE FROM CONTACT_GROUP");
        jdbcTemplate.update("DELETE FROM CONTACT");
        jdbcTemplate.update("DELETE FROM ADDRESS");

        Long firstId = jdbcTemplate.queryForObject("SELECT ID FROM COUNTRY ORDER BY ID LIMIT 1", Long.class);
        int deletedCount = countryRepository.delete(firstId);

        Assert.assertEquals(1, deletedCount);

        Integer countryCount = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM COUNTRY", Integer.class);
        Assert.assertEquals((Integer) 4, countryCount);
    }

    @Test
    public void updateTest() {
        Long firstId = jdbcTemplate.queryForObject("SELECT ID FROM COUNTRY ORDER BY ID LIMIT 1", Long.class);

        Country afganistanCountry = countryRepository.findOne(firstId);

        String newName = "NewName";
        String newIsoCode = "NIC";
        afganistanCountry.setName(newName);
        afganistanCountry.setIsoCode(newIsoCode);

        Country updatedCountry = countryRepository.update(afganistanCountry);

        Assert.assertEquals(newName, updatedCountry.getName());
        Assert.assertEquals(newName, jdbcTemplate.queryForObject("SELECT NAME FROM COUNTRY WHERE ID=?", String.class, firstId));
        Assert.assertEquals(newIsoCode, updatedCountry.getIsoCode());
        Assert.assertEquals(newIsoCode, jdbcTemplate.queryForObject("SELECT ISO_CODE FROM COUNTRY WHERE ID=?", String.class, firstId));
    }

    @Test
    public void createTest() {
        Country country = new Country();
        String newName = "NewName";
        String newIsoCode = "NIC";
        country.setName(newName);
        country.setIsoCode(newIsoCode);

        Country newCountry = countryRepository.create(country);

        Long lastId = jdbcTemplate.queryForObject("SELECT ID FROM COUNTRY ORDER BY ID DESC LIMIT 1", Long.class);

        Assert.assertEquals(lastId, newCountry.getId());
        Assert.assertEquals(newName, newCountry.getName());
        Assert.assertEquals(newName, jdbcTemplate.queryForObject("SELECT NAME FROM COUNTRY WHERE ID=?", String.class, lastId));
        Assert.assertEquals(newIsoCode, newCountry.getIsoCode());
        Assert.assertEquals(newIsoCode, jdbcTemplate.queryForObject("SELECT ISO_CODE FROM COUNTRY WHERE ID=?", String.class, lastId));
    }
}
