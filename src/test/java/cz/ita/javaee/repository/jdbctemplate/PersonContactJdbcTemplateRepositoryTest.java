package cz.ita.javaee.repository.jdbctemplate;

import java.util.List;

import cz.ita.javaee.model.Address;
import cz.ita.javaee.model.PersonContact;
import cz.ita.javaee.repository.api.AddressRepository;
import cz.ita.javaee.repository.api.CountryRepository;
import cz.ita.javaee.repository.api.PersonContactRepository;
import cz.ita.javaee.test.SpringContextTest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static cz.ita.javaee.model.Contact.ContactType.PERSON;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
@Rollback
@ActiveProfiles("jdbcTemplate")
public class PersonContactJdbcTemplateRepositoryTest extends SpringContextTest {

    private static final int FIRST_ID = 1;
    private static final int PERSON_CONTACT_COUNT = 2;

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private PersonContactRepository personContactRepository;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private CountryRepository countryRepository;

    @Test
    public void findOneTest() {
        //INSERT INTO PERSON_CONTACT(ID, FIRSTNAME, SURNAME, CONTACT_ID) VALUES(1, 'Jan', 'Novak', 1);
        //INSERT INTO CONTACT(ID, PHONE_NUMBER, EMAIL, ADDRESS_ID) VALUES(1, '+420000000000', 'jan.novak@noexistdomain.cz', 1);
        PersonContact firstPersonContact = personContactRepository.findOne(FIRST_ID);
        Assert.assertEquals("Jan", firstPersonContact.getFirstName());
        Assert.assertEquals("Novak", firstPersonContact.getSurname());
        Assert.assertEquals("+420000000000", firstPersonContact.getPhoneNumber());
        Assert.assertEquals("jan.novak@noexistdomain.cz", firstPersonContact.getEmail());
    }

    @Test
    public void findTest() {
        List<PersonContact> countries = personContactRepository.find(0, 10, "ID");
        Assert.assertThat(countries, Matchers.hasSize(PERSON_CONTACT_COUNT));
    }

    @Test
    public void findOffsetTest() {
        List<PersonContact> countries = personContactRepository.find(1, 10, "ID");
        Assert.assertThat(countries, Matchers.hasSize(PERSON_CONTACT_COUNT - 1));
    }

    @Test
    public void findLimitTest() {
        List<PersonContact> countries = personContactRepository.find(0, 1, "ID");
        Assert.assertThat(countries, Matchers.hasSize(1));
    }

    @Test
    public void deleteTest() {
        jdbcTemplate.update("DELETE FROM CONTACT_GROUP_CONTACT");
        jdbcTemplate.update("DELETE FROM CONTACT_GROUP");

        int deletedCount = personContactRepository.delete(FIRST_ID);

        Assert.assertEquals(1, deletedCount);

        Integer countryCount = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM CONTACT WHERE TYPE=?", Integer.class, PERSON.name());
        Assert.assertEquals(PERSON_CONTACT_COUNT - 1, (int) countryCount);
    }

    @Test
    public void updateTest() {

        //INSERT INTO CONTACT(ID, TYPE, FIRSTNAME, SURNAME, COMPANY_NAME, COMPANY_ID, VAT_ID, PHONE_NUMBER, EMAIL, ADDRESS_ID) VALUES(1, 'PERSON', 'Jan', 'Novak', NULL, NULL, NULL, '+420000000000', 'jan.novak@noexistdomain.cz', 1);
        PersonContact personContact = personContactRepository.findOne(FIRST_ID);

        String newFirstname = "NewFirstname";
        String newSurname = "NewSurname";
        String newPhoneNumber = "NewPhoneNumber";
        String newEmail = "NewEmail";
        Address newAddress = new Address();
        newAddress.setStreet("Street A");
        newAddress.setStreetNumber("1/2");
        newAddress.setCity("Jihlava");
        newAddress.setZipCode("90876");
        newAddress.setCountry(countryRepository.findOne(FIRST_ID));
        addressRepository.create(newAddress);

        personContact.setFirstName(newFirstname);
        personContact.setSurname(newSurname);
        personContact.setPhoneNumber(newPhoneNumber);
        personContact.setEmail(newEmail);
        personContact.setAddress(newAddress);

        PersonContact updatedContact = personContactRepository.update(personContact);

        Assert.assertEquals(newFirstname, updatedContact.getFirstName());
        Assert.assertEquals(newSurname, updatedContact.getSurname());
        Assert.assertEquals(newPhoneNumber, updatedContact.getPhoneNumber());
        Assert.assertEquals(newEmail, updatedContact.getEmail());
        Assert.assertEquals(newAddress.getId(), updatedContact.getAddress().getId());

        Assert.assertEquals(newFirstname, jdbcTemplate.queryForObject("SELECT FIRSTNAME FROM CONTACT WHERE ID=?", String.class, FIRST_ID));
        Assert.assertEquals(newSurname, jdbcTemplate.queryForObject("SELECT SURNAME FROM CONTACT WHERE ID=?", String.class, FIRST_ID));
        Assert.assertEquals(newPhoneNumber, jdbcTemplate.queryForObject("SELECT PHONE_NUMBER FROM CONTACT WHERE ID=?", String.class, FIRST_ID));
        Assert.assertEquals(newEmail, jdbcTemplate.queryForObject("SELECT EMAIL FROM CONTACT WHERE ID=?", String.class, FIRST_ID));
        Assert.assertEquals(newAddress.getId(), jdbcTemplate.queryForObject("SELECT ADDRESS_ID FROM CONTACT WHERE ID=?", Long.class, FIRST_ID));
    }

    @Test
    public void createTest() {
        PersonContact personContact = new PersonContact();

        String newFirstname = "NewFirstname";
        String newSurname = "NewSurname";
        String newPhoneNumber = "NewPhoneNumber";
        String newEmail = "NewEmail";

        Address newAddress = new Address();
        newAddress.setStreet("Street A");
        newAddress.setStreetNumber("1/2");
        newAddress.setCity("Jihlava");
        newAddress.setZipCode("90876");
        newAddress.setCountry(countryRepository.findOne(FIRST_ID));
        addressRepository.create(newAddress);

        personContact.setFirstName(newFirstname);
        personContact.setSurname(newSurname);
        personContact.setPhoneNumber(newPhoneNumber);
        personContact.setEmail(newEmail);
        personContact.setAddress(newAddress);

        PersonContact createdContact = personContactRepository.create(personContact);

        Long lastId = jdbcTemplate.queryForObject("SELECT ID FROM CONTACT ORDER BY ID DESC LIMIT 1", Long.class);

        Assert.assertEquals(lastId, createdContact.getId());
        Assert.assertEquals(newFirstname, createdContact.getFirstName());
        Assert.assertEquals(newSurname, createdContact.getSurname());
        Assert.assertEquals(newPhoneNumber, createdContact.getPhoneNumber());
        Assert.assertEquals(newEmail, createdContact.getEmail());
        Assert.assertEquals(newAddress.getId(), createdContact.getAddress().getId());

        Assert.assertEquals(newFirstname, jdbcTemplate.queryForObject("SELECT FIRSTNAME FROM CONTACT WHERE ID=?", String.class, lastId));
        Assert.assertEquals(newSurname, jdbcTemplate.queryForObject("SELECT SURNAME FROM CONTACT WHERE ID=?", String.class, lastId));
        Assert.assertEquals(newPhoneNumber, jdbcTemplate.queryForObject("SELECT PHONE_NUMBER FROM CONTACT WHERE ID=?", String.class, lastId));
        Assert.assertEquals(newEmail, jdbcTemplate.queryForObject("SELECT EMAIL FROM CONTACT WHERE ID=?", String.class, lastId));
        Assert.assertEquals(newAddress.getId(), jdbcTemplate.queryForObject("SELECT ADDRESS_ID FROM CONTACT WHERE ID=?", Long.class, lastId));
    }
}
