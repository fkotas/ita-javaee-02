package cz.ita.javaee.repository.jdbctemplate;

import java.util.List;

import cz.ita.javaee.model.Address;
import cz.ita.javaee.model.CompanyContact;
import cz.ita.javaee.repository.api.AddressRepository;
import cz.ita.javaee.repository.api.CompanyContactRepository;
import cz.ita.javaee.repository.api.CountryRepository;
import cz.ita.javaee.test.SpringContextTest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import static cz.ita.javaee.model.Contact.ContactType.COMPANY;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Transactional
@Rollback
@ActiveProfiles("jdbcTemplate")
//@ActiveProfiles("namedParameterJdbcTemplate")
public class CompanyContactJdbcTemplateRepositoryTest extends SpringContextTest {

    private static final int FIRST_ID = 3;
    private static final int COMPANY_CONTACT_COUNT = 2;

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private CompanyContactRepository companyContactRepository;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private CountryRepository countryRepository;

    @Test
    public void findOneTest() {
        //INSERT INTO CONTACT(ID, TYPE, FIRSTNAME, SURNAME, COMPANY_NAME, COMPANY_ID, VAT_ID, PHONE_NUMBER, EMAIL, ADDRESS_ID)
        // VALUES(3, 'COMPANY', NULL, NULL, 'AFG Mail', '999887', 'AF999887', '+660987643234', 'info22@afgmail.com', 3);
        CompanyContact firstCompanyContact = companyContactRepository.findOne(FIRST_ID);
        Assert.assertEquals("AFG Mail", firstCompanyContact.getCompanyName());
        Assert.assertEquals("999887", firstCompanyContact.getCompanyId());
        Assert.assertEquals("AF999887", firstCompanyContact.getVatId());
        Assert.assertEquals("+660987643234", firstCompanyContact.getPhoneNumber());
        Assert.assertEquals("info22@afgmail.com", firstCompanyContact.getEmail());
    }

    @Test
    public void findTest() {
        List<CompanyContact> countries = companyContactRepository.find(0, 10, "ID");
        Assert.assertThat(countries, Matchers.hasSize(COMPANY_CONTACT_COUNT));
    }

    @Test
    public void findOffsetTest() {
        List<CompanyContact> countries = companyContactRepository.find(1, 10, "ID");
        Assert.assertThat(countries, Matchers.hasSize(COMPANY_CONTACT_COUNT - 1));
    }

    @Test
    public void findLimitTest() {
        List<CompanyContact> countries = companyContactRepository.find(0, 1, "ID");
        Assert.assertThat(countries, Matchers.hasSize(1));
    }

    @Test
    public void deleteTest() {
        jdbcTemplate.update("DELETE FROM CONTACT_GROUP_CONTACT");
        jdbcTemplate.update("DELETE FROM CONTACT_GROUP");

        int deletedCount = companyContactRepository.delete(FIRST_ID);

        Assert.assertEquals(1, deletedCount);

        Integer countryCount = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM CONTACT WHERE TYPE=?", Integer.class, COMPANY.name());
        Assert.assertEquals(COMPANY_CONTACT_COUNT - 1, (int) countryCount);
    }

    @Test
    public void updateTest() {

        CompanyContact companyContact = companyContactRepository.findOne(FIRST_ID);

        String newCompanyName = "NewCompanyName";
        String newCompanyId = "123456";
        String newVatId = "NewVatId";
        String newPhoneNumber = "NewPhoneNumber";
        String newEmail = "NewEmail";
        Address newAddress = new Address();
        newAddress.setStreet("Street A");
        newAddress.setStreetNumber("1/2");
        newAddress.setCity("Jihlava");
        newAddress.setZipCode("90876");
        newAddress.setCountry(countryRepository.findOne(FIRST_ID));
        addressRepository.create(newAddress);

        companyContact.setCompanyName(newCompanyName);
        companyContact.setCompanyId(newCompanyId);
        companyContact.setVatId(newVatId);
        companyContact.setPhoneNumber(newPhoneNumber);
        companyContact.setEmail(newEmail);
        companyContact.setAddress(newAddress);

        CompanyContact updatedContact = companyContactRepository.update(companyContact);

        Assert.assertEquals(newCompanyName, updatedContact.getCompanyName());
        Assert.assertEquals(newCompanyId, updatedContact.getCompanyId());
        Assert.assertEquals(newVatId, updatedContact.getVatId());
        Assert.assertEquals(newPhoneNumber, updatedContact.getPhoneNumber());
        Assert.assertEquals(newEmail, updatedContact.getEmail());
        Assert.assertEquals(newAddress.getId(), updatedContact.getAddress().getId());

        Assert.assertEquals(newCompanyName, jdbcTemplate.queryForObject("SELECT COMPANY_NAME FROM CONTACT WHERE ID=?", String.class, FIRST_ID));
        Assert.assertEquals(newCompanyId, jdbcTemplate.queryForObject("SELECT COMPANY_ID FROM CONTACT WHERE ID=?", String.class, FIRST_ID));
        Assert.assertEquals(newVatId, jdbcTemplate.queryForObject("SELECT VAT_ID FROM CONTACT WHERE ID=?", String.class, FIRST_ID));
        Assert.assertEquals(newPhoneNumber, jdbcTemplate.queryForObject("SELECT PHONE_NUMBER FROM CONTACT WHERE ID=?", String.class, FIRST_ID));
        Assert.assertEquals(newEmail, jdbcTemplate.queryForObject("SELECT EMAIL FROM CONTACT WHERE ID=?", String.class, FIRST_ID));
        Assert.assertEquals(newAddress.getId(), jdbcTemplate.queryForObject("SELECT ADDRESS_ID FROM CONTACT WHERE ID=?", Long.class, FIRST_ID));
    }

    @Test
    public void createTest() {
        CompanyContact companyContact = new CompanyContact();

        String newCompanyName = "NewCompanyName";
        String newCompanyId = "123456";
        String newVatId = "NewVatId";
        String newPhoneNumber = "NewPhoneNumber";
        String newEmail = "NewEmail";

        Address newAddress = new Address();
        newAddress.setStreet("Street A");
        newAddress.setStreetNumber("1/2");
        newAddress.setCity("Jihlava");
        newAddress.setZipCode("90876");
        newAddress.setCountry(countryRepository.findOne(FIRST_ID));
        addressRepository.create(newAddress);

        companyContact.setCompanyName(newCompanyName);
        companyContact.setCompanyId(newCompanyId);
        companyContact.setVatId(newVatId);
        companyContact.setPhoneNumber(newPhoneNumber);
        companyContact.setEmail(newEmail);
        companyContact.setAddress(newAddress);

        CompanyContact createdContact = companyContactRepository.create(companyContact);

        Long lastId = jdbcTemplate.queryForObject("SELECT ID FROM CONTACT ORDER BY ID DESC LIMIT 1", Long.class);

        Assert.assertEquals(lastId, createdContact.getId());
        Assert.assertEquals(newCompanyName, createdContact.getCompanyName());
        Assert.assertEquals(newCompanyId, createdContact.getCompanyId());
        Assert.assertEquals(newVatId, createdContact.getVatId());
        Assert.assertEquals(newPhoneNumber, createdContact.getPhoneNumber());
        Assert.assertEquals(newEmail, createdContact.getEmail());
        Assert.assertEquals(newAddress.getId(), createdContact.getAddress().getId());

        Assert.assertEquals(newCompanyName, jdbcTemplate.queryForObject("SELECT COMPANY_NAME FROM CONTACT WHERE ID=?", String.class, lastId));
        Assert.assertEquals(newCompanyId, jdbcTemplate.queryForObject("SELECT COMPANY_ID FROM CONTACT WHERE ID=?", String.class, lastId));
        Assert.assertEquals(newVatId, jdbcTemplate.queryForObject("SELECT VAT_ID FROM CONTACT WHERE ID=?", String.class, lastId));
        Assert.assertEquals(newPhoneNumber, jdbcTemplate.queryForObject("SELECT PHONE_NUMBER FROM CONTACT WHERE ID=?", String.class, lastId));
        Assert.assertEquals(newEmail, jdbcTemplate.queryForObject("SELECT EMAIL FROM CONTACT WHERE ID=?", String.class, lastId));
        Assert.assertEquals(newAddress.getId(), jdbcTemplate.queryForObject("SELECT ADDRESS_ID FROM CONTACT WHERE ID=?", Long.class, lastId));
    }
}
