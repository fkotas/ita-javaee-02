package cz.ita.javaee.service;

import java.util.List;

import cz.ita.javaee.model.Address;
import cz.ita.javaee.model.CompanyContact;
import cz.ita.javaee.model.Country;
import cz.ita.javaee.model.PersonContact;
import cz.ita.javaee.repository.api.AddressRepository;
import cz.ita.javaee.repository.api.CountryRepository;
import cz.ita.javaee.service.api.ContactService;
import cz.ita.javaee.test.SpringContextTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

@Rollback
public class ContactServiceTest extends SpringContextTest {

    private static final long AFGANISGAN_COUNTRY_ID = 1;
    private static final int ADDRESS_COUNT = 4;

    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private ContactService contactService;
    @Autowired
    private AddressRepository addressRepository;

    @Test
    @Transactional
    public void savePersonContactSuccess() {
        Country afghanistanCountry = countryRepository.findOne(AFGANISGAN_COUNTRY_ID);

        Address address = new Address("Alca street", "23", "Prague", "987654", afghanistanCountry);
        PersonContact personContact = new PersonContact("John", "smith", "+421987876543", "john.smith@ggmail.com", address);
        contactService.savePersonContact(personContact);

        Assert.assertNotNull(personContact.getId());
        Assert.assertNotNull(personContact.getAddress().getId());
    }

    @Test
    public void savePersonContactCheckTransaction() {
        Country afghanistanCountry = countryRepository.findOne(AFGANISGAN_COUNTRY_ID);


        Address address = new Address("Alca street", "23", "Prague", "987654", afghanistanCountry);
        PersonContact personContact = new PersonContact(null, "smith", "+421987876543", "john.smith@ggmail.com", address);

        try {
            contactService.savePersonContact(personContact);
            Assert.fail("Should failed for NPE.");
        } catch (Exception e) {
            // it's ok, skip exception
        }

        List<Address> addresses = addressRepository.find(0, ADDRESS_COUNT * 2, "ID");
        Assert.assertEquals(ADDRESS_COUNT, addresses.size());
    }

    @Test
    @Transactional
    public void saveCompanyContactSuccess() {
        Country afghanistanCountry = countryRepository.findOne(AFGANISGAN_COUNTRY_ID);

        Address address = new Address("Alca street", "23", "Prague", "987654", afghanistanCountry);
        CompanyContact companyContact = new CompanyContact("Company A", "000000", "AF000000", "+421987876543", "info@compA.af", address);
        contactService.saveCompanyContact(companyContact);

        Assert.assertNotNull(companyContact.getId());
        Assert.assertNotNull(companyContact.getAddress().getId());
    }

    @Test
    public void saveCompanyContactCheckTransaction() {
        Country afghanistanCountry = countryRepository.findOne(AFGANISGAN_COUNTRY_ID);

        Address address = new Address("Alca street", "23", "Prague", "987654", afghanistanCountry);
        CompanyContact companyContact = new CompanyContact(null, "000000", "AF000000", "+421987876543", "info@compA.af", address);
        List<Address> addresses = addressRepository.find(0, ADDRESS_COUNT * 2, "ID");

        try {
            contactService.saveCompanyContact(companyContact);
            Assert.fail("Should failed for NPE.");
        } catch (Exception e) {
            // it's ok, skip exception
        }
        addresses = addressRepository.find(0, ADDRESS_COUNT * 2, "ID");
        Assert.assertEquals(ADDRESS_COUNT, addresses.size());
    }
}
