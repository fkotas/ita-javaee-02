package cz.ita.javaee.service.api;

import cz.ita.javaee.model.CompanyContact;
import cz.ita.javaee.model.PersonContact;
import cz.ita.javaee.repository.api.AddressRepository;
import cz.ita.javaee.repository.api.CompanyContactRepository;
import cz.ita.javaee.repository.api.PersonContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Franta on 2.4.2017.
 */
@Service
public class JdbcContactService implements ContactService {

    @Autowired
    private PersonContactRepository personContactRepository;

    @Autowired
    private CompanyContactRepository companyContactRepository;

    @Autowired
    private AddressRepository addressRepository;

    /**
     * Persist new PersonContact with address into database.
     * @param contact to be persisted.
     * @return persisted contact with filled id.
     */
    @Override
    @Transactional
    public PersonContact savePersonContact(PersonContact contact) {
        addressRepository.create(contact.getAddress());
        return personContactRepository.create(contact);
    }

    /**
     * Persist new CompanyContact with address into database.
     * @param contact to be persisted.
     * @return persisted contact with filled id.
     */
    @Override
    @Transactional
    public CompanyContact saveCompanyContact(CompanyContact contact) {
        addressRepository.create(contact.getAddress());
        return companyContactRepository.create(contact);
    }
}
