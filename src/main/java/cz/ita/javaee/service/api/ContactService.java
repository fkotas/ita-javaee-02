package cz.ita.javaee.service.api;

import cz.ita.javaee.model.CompanyContact;
import cz.ita.javaee.model.PersonContact;

public interface ContactService {

    /**
     * Persist new PersonContact with address into database.
     * @param contact to be persisted.
     * @return persisted contact with filled id.
     */
    PersonContact savePersonContact(PersonContact contact);

    /**
     * Persist new CompanyContact with address into database.
     * @param contact to be persisted.
     * @return persisted contact with filled id.
     */
    CompanyContact saveCompanyContact(CompanyContact contact);
}
