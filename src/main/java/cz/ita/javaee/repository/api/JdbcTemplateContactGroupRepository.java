package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.CompanyContact;
import cz.ita.javaee.model.Contact;
import cz.ita.javaee.model.ContactGroup;
import cz.ita.javaee.model.PersonContact;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.sql.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Franta on 31.3.2017.
 */
@Repository("jdbcTemplateContactGroupRepository")
@Profile("jdbcTemplate")
public class JdbcTemplateContactGroupRepository implements ContactGroupRepository {


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private JdbcTemplatePersonContactRepository jdbcTemplatePersonContactRepository;

    @Autowired
    private JdbcTemplateCompanyContactRepository jdbcTemplateCompanyContactRepository;

    private final ContactGroupRowMapper ROW_MAPPER = new ContactGroupRowMapper();

    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcTemplateContactGroupRepository.class);

    /**
     * Find entity by id.
     * @param id identifier number of entity T
     * @return founded entity T
     */
    @Override
    public ContactGroup findOne(long id) {
        ContactGroup contactGroup = jdbcTemplate.queryForObject("select * from contact_group where id = ?", ROW_MAPPER, id);
        contactGroup.setContacts(findContactsForContactGroup(id));
        return contactGroup;
    }

    /**
     * Find entities.
     * @param offset is first record
     * @param limit is last record
     * @param orderBy is sort column name
     * @return list of founded entities
     */
    @Override
    public List<ContactGroup> find(int offset, int limit, final String orderBy) {
        List<ContactGroup> result = jdbcTemplate.query("select * from contact_group order by "+orderBy+" limit ? offset ?", ROW_MAPPER, limit, offset);
        for(ContactGroup contactGroup : result) {
            contactGroup.setContacts(findContactsForContactGroup(contactGroup.getId()));
        }
        return result;
    }

    /**
     * Finds all contacts for a ContactGroup.
     * @param contactGroupId id of ContactGroup we're findind contacts for
     * @return Set containing contacts for the ContactGroup.
     */
    private Set<Contact> findContactsForContactGroup(long contactGroupId) {
        List<Long> contactIds = jdbcTemplate.queryForList("select contact_id from contact_group_contact where contact_group_id = ?", Long.class, contactGroupId);
        if(CollectionUtils.isEmpty(contactIds)) return new HashSet<Contact>();

        //musime udelat 2 dotazy, jeden pro kontakty typu PERSON a druhy pro typ COMPANY
        StringBuilder sql = new StringBuilder("select * from contact where type = 'PERSON' and id in (");
        for(Long contactId : contactIds) {
            sql.append(contactId);
            sql.append(",");
        }
        //smazeme posledni carku v sql dotazu a vlozime uzavirajici zavorku bloku in
        sql.deleteCharAt(sql.length()-1);
        sql.append(")");
        //vyhledani pro typ PERSON
        List<PersonContact> personContacts = jdbcTemplatePersonContactRepository.findByIds(sql.toString());
        //vyhledani pro typ COMPANY
        sql.replace(36,42,"COMPANY");
        List<CompanyContact> companyContacts = jdbcTemplateCompanyContactRepository.findByIds(sql.toString());

        //slouceni a vraceni vysledku obou dotazu
        Set<Contact> result = new HashSet<>();
        result.addAll(personContacts);
        result.addAll(companyContacts);
        return result;
    }

    /**
     * Delete entity.
     * @param id identifier number of entity T
     * @return number of deleted records
     */
    @Override
    public int delete(long id) {
        jdbcTemplate.update("delete from contact_group_contact where contact_group_id = ?", id);
        int rowsDeleted = jdbcTemplate.update("delete from contact_group where id = ?", id);
        return rowsDeleted;
    }

    /**
     * @param entity entity with updated field identified by id
     * @return updated entity from repository
     */
    @Override
    public ContactGroup update(final ContactGroup entity) {
        jdbcTemplate.update("update contact_group set name = ? where id = ?", entity.getName(), entity.getId());

        Set<Contact> currentContacts = entity.getContacts();

        //smazeme zaznamy, co v contact_group_contact prebyvaji
        Set<Contact> oldContacts = findContactsForContactGroup(entity.getId());
        for(Contact contact : oldContacts) {
            if(!currentContacts.contains(contact)) {
                jdbcTemplate.update("delete from contact_group_contact where contact_id = ? and contact_group_id = ?", contact.getId(), entity.getId());
            }
        }

        //pridame zaznamy, co v contact_group_contact chybi
        for(Contact contact : currentContacts) {
            Integer i = jdbcTemplate.queryForObject("select count(*) from contact_group_contact where contact_group_id = ? and contact_id = ?", Integer.class, entity.getId(), contact.getId());
            if(i<1) {
                LOGGER.debug("Zaznam nebyl v tabulce, pridan zaznam s contactId "+contact.getId()+" a contact_group_id "+entity.getId());
                jdbcTemplate.update("insert into contact_group_contact (contact_group_id,contact_id) values (?,?)", entity.getId(), contact.getId());
            }
        }

        return findOne(entity.getId());
    }

    /**
     * @param entity entity class to store in repository
     * @return new persisted record with filled ID
     */
    @Override
    public ContactGroup create(final ContactGroup entity) {
        //pridani saznamu do tabulky contact_group
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement("insert into contact_group (name) values (?)", Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1, entity.getName());
                        return ps;
                    }
                }, keyHolder);
        entity.setId(keyHolder.getKey().longValue());

        //pridani vsech zaznamu do tabulky contact_group_contact
        for(Contact contact : entity.getContacts()) {
            jdbcTemplate.update("insert into contact_group_contact (contact_group_id,contact_id) values (?,?)", entity.getId(), contact.getId());
        }

        return entity;
    }

    private class ContactGroupRowMapper implements RowMapper<ContactGroup> {
        @Override
        public ContactGroup mapRow(ResultSet rs, int rowNum) throws SQLException {
            ContactGroup contactGroup = new ContactGroup();
            contactGroup.setId(rs.getLong(1));
            contactGroup.setName(rs.getString(2));
            return contactGroup;
        }
    }
}


