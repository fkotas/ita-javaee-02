package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.ContactGroup;

public interface ContactGroupRepository extends EntityRepository<ContactGroup> {
}
