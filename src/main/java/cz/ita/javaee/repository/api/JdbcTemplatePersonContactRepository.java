package cz.ita.javaee.repository.api;

import com.google.common.base.Preconditions;
import cz.ita.javaee.model.PersonContact;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;

/**
 * Created by Franta on 31.3.2017.
 */
@Repository("jdbcTemplatePersonContactRepository")
@Profile("jdbcTemplate")
public class JdbcTemplatePersonContactRepository implements PersonContactRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private JdbcTemplateAddressRepository jdbcTemplateAddressRepository;

    private PersonContactRowMapper ROW_MAPPER = new PersonContactRowMapper();

    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcTemplatePersonContactRepository.class);

    /**
     * Find entity by id.
     * @param id identifier number of entity T
     * @return founded entity T
     */
    @Override
    public PersonContact findOne(long id) {
        PersonContact contact = jdbcTemplate.queryForObject("select * from contact where id = ? and type = 'PERSON'", ROW_MAPPER, id);
        LOGGER.debug("Vraceno: "+contact.toString());
        return contact;
    }

    /**
     * Find entities.
     * @param offset is first record
     * @param limit is last record
     * @param orderBy is sort column name
     * @return list of founded entities
     */
    @Override
    public List<PersonContact> find(int offset, int limit, final String orderBy) {
        List<PersonContact> result = jdbcTemplate.query("select * from contact where type = 'PERSON' order by "+orderBy+" limit ? offset ?", ROW_MAPPER, limit, offset);
        for(PersonContact contact : result) {
            LOGGER.debug("Vraceno: "+contact.toString());
        }
        return result;
    }

    public List<PersonContact> findByIds(String sql) {
        return jdbcTemplate.query(sql, ROW_MAPPER);
    }

    /**
     * Delete entity.
     * @param id identifier number of entity T
     * @return number of deleted records
     */
    @Override
    public int delete(long id) {
        int rowsDeleted = jdbcTemplate.update("delete from contact where id = ? and type = 'PERSON'", id);
        LOGGER.debug("Smazano zaznamu: "+rowsDeleted);
        return rowsDeleted;
    }

    /**
     * @param entity entity with updated field identified by id
     * @return updated entity from repository
     */
    @Override
    public PersonContact update(final PersonContact entity) {
        checkAttributes(entity);
        jdbcTemplate.update("update contact set firstname = ?, surname = ?, email = ?, phone_number = ?, address_id = ? where id = ? and type='PERSON'", entity.getFirstName(), entity.getSurname(), entity.getEmail(), entity.getPhoneNumber(), entity.getAddress().getId(), entity.getId());
        return findOne(entity.getId());
    }

    /**
     * @param entity entity class to store in repository
     * @return new persisted record with filled ID
     */
    @Override
    public PersonContact create(final PersonContact entity) {
        checkAttributes(entity);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement("insert into contact (type, firstname, surname, email, phone_number, address_id) values (?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1,"PERSON");
                        ps.setString(2, entity.getFirstName());
                        ps.setString(3, entity.getSurname());
                        ps.setString(4, entity.getEmail());
                        ps.setString(5, entity.getPhoneNumber());
                        ps.setLong(6, entity.getAddress().getId());
                        return ps;
                    }
                }, keyHolder);
        entity.setId(keyHolder.getKey().longValue());
        return entity;
    }

    /**
     * Checks whether all mandatory fields of the PersonContact are filled (not null).
     * @param entity PersonContact on which to do the checks.
     *
     * @throws NullPointerException if any of the checks fails.
     */
    private void checkAttributes(PersonContact entity) {
        Preconditions.checkNotNull(entity.getPhoneNumber());
        Preconditions.checkNotNull(entity.getAddress().getId());
        Preconditions.checkNotNull(entity.getFirstName());
        Preconditions.checkNotNull(entity.getSurname());
    }

    private class PersonContactRowMapper implements RowMapper<PersonContact> {
        @Override
        public PersonContact mapRow(ResultSet rs, int rowNum) throws SQLException {
            PersonContact contact = new PersonContact();
            contact.setId(rs.getLong(1));
            contact.setFirstName((rs.getString(3)));
            contact.setSurname(rs.getString(4));
            contact.setEmail(rs.getString(6));
            contact.setPhoneNumber(rs.getString(9));
            contact.setAddress(jdbcTemplateAddressRepository.findOne(rs.getLong(10)));
            return contact;
        }
    }    
}
