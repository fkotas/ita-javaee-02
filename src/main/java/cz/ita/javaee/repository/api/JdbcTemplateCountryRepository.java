package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;

/**
 * Created by Franta on 29.3.2017.
 */
@Repository("jdbcTemplateCountryRepository")
@Profile("jdbcTemplate")
public class JdbcTemplateCountryRepository implements CountryRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final CountryRowMapper ROW_MAPPER = new CountryRowMapper();

    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcTemplateCountryRepository.class);

    /**
     * Find entity by id.
     * @param id identifier number of entity T
     * @return founded entity T
     */
    @Override
    public Country findOne(long id) {
        Country country = jdbcTemplate.queryForObject("select * from country where id = ?", ROW_MAPPER, id);
        return country;
    }

    /**
     * Find entities.
     * @param offset is first record
     * @param limit is last record
     * @param orderBy is sort column name
     * @return list of founded entities
     */
    @Override
    public List<Country> find(int offset, int limit, final String orderBy) {
        List<Country> result = jdbcTemplate.query("select * from country order by "+orderBy+" limit ? offset ?", ROW_MAPPER, limit, offset);
        return result;
    }

    /**
     * Delete entity.
     * @param id identifier number of entity T
     * @return number of deleted records
     */
    @Override
    public int delete(long id) {
        int rowsDeleted = jdbcTemplate.update("delete from country where id = ?", id);
        return rowsDeleted;
    }

    /**
     * @param entity entity with updated field identified by id
     * @return updated entity from repository
     */
    @Override
    public Country update(final Country entity) {
        jdbcTemplate.update("update country set name = ?, iso_code = ? where id = ?", entity.getName(), entity.getIsoCode(), entity.getId());
        return findOne(entity.getId());
    }

    /**
     * @param entity entity class to store in repository
     * @return new persisted record with filled ID
     */
    @Override
    public Country create(final Country entity) {

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement("insert into country (name, iso_code) values (?,?)", Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1, entity.getName());
                        ps.setString(2, entity.getIsoCode());
                        return ps;
                    }
                }, keyHolder);
        entity.setId(keyHolder.getKey().longValue());
        return entity;

        //Zkousel jsem nejprve nize zakomentovanou variantu, podle prikladu v prezentaci, ale nesla mi rozjet.
        //Hazelo mi to vyjimku java.sql.SQLSyntaxErrorException: user lacks privilege or object not found: id in statement [insert into country (name, iso_code) values (?,?)]
        //Stravil jsem nejaky cas hledanim reseni na netu, ale nepodarilo se mi nic objevit - zdalo se mi, ze mam prikaz napsany presne stejne, jako v nekolika prikladech,
        //ktere jsem nasel. Nevis, v cem by mohl byt problem?

        /*
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement("insert into country (name, iso_code) values (?,?)", new String[] {"id"});
                        ps.setString(1, entity.getName());
                        ps.setString(2, entity.getIsoCode());
                        return ps;
                    }
                }, keyHolder);
        entity.setId(keyHolder.getKey().longValue());
        return entity;
        */
    }

    private class CountryRowMapper implements RowMapper<Country> {
        @Override
        public Country mapRow(ResultSet rs, int rowNum) throws SQLException {
            Country country = new Country();
            country.setId(rs.getLong(1));
            country.setName(rs.getString(2));
            country.setIsoCode(rs.getString(3));
            return country;
        }
    }

}


