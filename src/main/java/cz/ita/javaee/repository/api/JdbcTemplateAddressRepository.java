package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;

/**
 * Created by Franta on 31.3.2017.
 */
@Repository("jdbcTemplateAddressRepository")
@Profile("jdbcTemplate")
public class JdbcTemplateAddressRepository implements AddressRepository {


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private JdbcTemplateCountryRepository jdbcTemplateCountryRepository;

    private final AddressRowMapper ROW_MAPPER = new AddressRowMapper();

    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcTemplateAddressRepository.class);

    /**
     * Find entity by id.
     * @param id identifier number of entity T
     * @return founded entity T
     */
    @Override
    public Address findOne(long id) {
        Address address = jdbcTemplate.queryForObject("select * from address where id = ?", ROW_MAPPER, id);
        return address;
    }

    /**
     * Find entities.
     * @param offset is first record
     * @param limit is last record
     * @param orderBy is sort column name
     * @return list of founded entities
     */
    @Override
    public List<Address> find(int offset, int limit, final String orderBy) {
        List<Address> result = jdbcTemplate.query("select * from address order by "+orderBy+" limit ? offset ?", ROW_MAPPER, limit, offset);
        return result;
    }

    /**
     * Delete entity.
     * @param id identifier number of entity T
     * @return number of deleted records
     */
    @Override
    public int delete(long id) {
        int rowsDeleted = jdbcTemplate.update("delete from address where id = ?", id);
        return rowsDeleted;
    }

    /**
     * @param entity entity with updated field identified by id
     * @return updated entity from repository
     */
    @Override
    public Address update(final Address entity) {
        jdbcTemplate.update("update address set street = ?, street_number = ?, city = ?, zip_code = ?, country_id = ? where id = ?", entity.getStreet(), entity.getStreetNumber(), entity.getCity(), entity.getZipCode(), entity.getCountry().getId(), entity.getId());
        return findOne(entity.getId());
    }

    /**
     * @param entity entity class to store in repository
     * @return new persisted record with filled ID
     */
    @Override
    public Address create(final Address entity) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement("insert into address (street, street_number, city, zip_code, country_id) values (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1, entity.getStreet());
                        ps.setString(2, entity.getStreetNumber());
                        ps.setString(3, entity.getCity());
                        ps.setString(4, entity.getZipCode());
                        ps.setLong(5, entity.getCountry().getId());
                        return ps;
                    }
                }, keyHolder);
        entity.setId(keyHolder.getKey().longValue());
        return entity;
    }

    class AddressRowMapper implements RowMapper<Address> {
        @Override
        public Address mapRow(ResultSet rs, int rowNum) throws SQLException {
            Address address = new Address();
            address.setId(rs.getLong(1));
            address.setStreet(rs.getString(2));
            address.setStreetNumber(rs.getString(3));
            address.setCity(rs.getString(4));
            address.setZipCode(rs.getString(5));
            address.setCountry(jdbcTemplateCountryRepository.findOne(rs.getLong(6)));
            return address;
        }
    }
}


