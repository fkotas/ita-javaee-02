package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Franta on 1.4.2017.
 */
@Repository
@Profile("namedParameterJdbcTemplate")
public class NamedParamTemplateAddressRepository implements AddressRepository {

    @Autowired
    private NamedParameterJdbcTemplate namedParamTemplate;

    @Autowired
    private NamedParamTemplateCountryRepository namedParamTemplateCountryRepository;

    private final AddressRowMapper ROW_MAPPER = new AddressRowMapper();

    private static final Logger LOGGER = LoggerFactory.getLogger(NamedParamTemplateAddressRepository.class);
    /**
     * Find entity by id.
     * @param id identifier number of entity T
     * @return founded entity T
     */
    @Override
    public Address findOne(long id) {
        String sql = "select * from address where id = :id";
        SqlParameterSource params = new MapSqlParameterSource("id", id);
        Address address = namedParamTemplate.queryForObject(sql, params, ROW_MAPPER);
        LOGGER.debug("Vraceno z db: "+address.toString());
        return address;
    }

    /**
     * Find entities.
     * @param offset is first record
     * @param limit is last record
     * @param orderBy is sort column name
     * @return list of founded entities
     */
    @Override
    public List<Address> find(int offset, int limit, final String orderBy) {
        String sql = "select * from address order by "+orderBy+" limit :limit offset :offset";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("limit", limit);
        params.addValue("offset", offset);
        return namedParamTemplate.query(sql, params, ROW_MAPPER);
    }

    /**
     * Delete entity.
     * @param id identifier number of entity T
     * @return number of deleted records
     */
    public int delete(long id) {
        String sql = "delete from address where id = :id";
        SqlParameterSource params = new MapSqlParameterSource("id", id);
        return namedParamTemplate.update(sql, params);
    }

    /**
     * @param entity entity with updated field identified by id
     * @return updated entity from repository
     */
    public Address update(final Address entity) {
        String sql = "update address set street = :street, street_number = :streetNumber, city = :city, zip_code = :zipCode, country_id = :countryId where id = :id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("street", entity.getStreet());
        params.addValue("streetNumber", entity.getStreetNumber());
        params.addValue("city", entity.getCity());
        params.addValue("zipCode", entity.getZipCode());
        params.addValue("countryId", entity.getCountry().getId());
        params.addValue("id", entity.getId());
        namedParamTemplate.update(sql, params);
        return findOne(entity.getId());
    }

    /**
     * @param entity entity class to store in repository
     * @return new persisted record with filled ID
     */
    public Address create(final Address entity) {
        String sql = "insert into address (street, street_number, city, zip_code, country_id) values (:street, :streetNumber, :city, :zipCode, :countryId)";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("street", entity.getStreet());
        params.addValue("streetNumber", entity.getStreetNumber());
        params.addValue("city", entity.getCity());
        params.addValue("zipCode", entity.getZipCode());
        params.addValue("countryId", entity.getCountry().getId());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParamTemplate.update(sql, params, keyHolder);
        entity.setId(keyHolder.getKey().longValue());
        return entity;
    }

    private class AddressRowMapper implements RowMapper<Address> {
        @Override
        public Address mapRow(ResultSet rs, int rowNum) throws SQLException {
            Address address = new Address();
            address.setId(rs.getLong(1));
            address.setStreet(rs.getString(2));
            address.setStreetNumber(rs.getString(3));
            address.setCity(rs.getString(4));
            address.setZipCode(rs.getString(5));
            address.setCountry(namedParamTemplateCountryRepository.findOne(rs.getLong(6)));
            return address;
        }
    }
}
