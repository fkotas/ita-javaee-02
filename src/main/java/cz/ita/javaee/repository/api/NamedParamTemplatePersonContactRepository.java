package cz.ita.javaee.repository.api;

import com.google.common.base.Preconditions;
import cz.ita.javaee.model.PersonContact;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Franta on 1.4.2017.
 */
@Repository
@Profile("namedParameterJdbcTemplate")
public class NamedParamTemplatePersonContactRepository implements PersonContactRepository {
    @Autowired
    private NamedParameterJdbcTemplate namedParamTemplate;

    @Autowired
    private NamedParamTemplateAddressRepository namedParamTemplateAddressRepository;

    private final PersonContactRowMapper ROW_MAPPER = new PersonContactRowMapper();

    private static final Logger LOGGER = LoggerFactory.getLogger(NamedParamTemplatePersonContactRepository.class);

    /**
     * Find entity by id.
     * @param id identifier number of entity T
     * @return founded entity T
     */
    @Override
    public PersonContact findOne(long id) {
        String sql = "select * from contact where id = :id and type = 'PERSON'";
        SqlParameterSource params = new MapSqlParameterSource("id", id);
        return namedParamTemplate.queryForObject(sql, params, ROW_MAPPER);
    }

    /**
     * Find entities.
     * @param offset is first record
     * @param limit is last record
     * @param orderBy is sort column name
     * @return list of founded entities
     */
    @Override
    public List<PersonContact> find(int offset, int limit, final String orderBy) {
        String sql = "select * from contact where type = 'PERSON' order by "+orderBy+" limit :limit offset :offset";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("limit", limit);
        params.addValue("offset", offset);
        return namedParamTemplate.query(sql, params, ROW_MAPPER);
    }

    public List<PersonContact> findByIds(String sql) {
        return namedParamTemplate.query(sql, ROW_MAPPER);
    }

    /**
     * Delete entity.
     * @param id identifier number of entity T
     * @return number of deleted records
     */
    @Override
    public int delete(long id) {
        String sql = "delete from contact where id = :id and type = 'PERSON'";
        SqlParameterSource params = new MapSqlParameterSource("id", id);
        return namedParamTemplate.update(sql, params);
    }

    /**
     * @param entity entity with updated field identified by id
     * @return updated entity from repository
     */
    @Override
    public PersonContact update(final PersonContact entity) {
        checkAttributes(entity);
        String sql = "update contact set firstName = :firstName, surname = :surname, email = :email, phone_number = :phoneNumber, address_id = :addressId where id = :id and type = 'PERSON'";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("firstName", entity.getFirstName());
        params.addValue("surname", entity.getSurname());
        params.addValue("email", entity.getEmail());
        params.addValue("phoneNumber", entity.getPhoneNumber());
        params.addValue("addressId", entity.getAddress().getId());
        params.addValue("id", entity.getId());
        namedParamTemplate.update(sql, params);
        return findOne(entity.getId());
    }

    /**
     * @param entity entity class to store in repository
     * @return new persisted record with filled ID
     */
    @Override
    public PersonContact create(final PersonContact entity) {
        checkAttributes(entity);
        String sql = "insert into contact (type, firstname, surname, email, phone_number, address_id) values (:type, :firstName, :surname, :email, :phoneNumber, :addressId)";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("type", "PERSON");
        params.addValue("firstName", entity.getFirstName());
        params.addValue("surname", entity.getSurname());
        params.addValue("email", entity.getEmail());
        params.addValue("phoneNumber", entity.getPhoneNumber());
        params.addValue("addressId", entity.getAddress().getId());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParamTemplate.update(sql, params, keyHolder);
        entity.setId(keyHolder.getKey().longValue());
        return entity;
    }

    /**
     * Checks whether all mandatory fields of the PersonContact are filled (not null).
     * @param entity PersonContact on which to do the checks.
     *
     * @throws NullPointerException if any of the checks fails.
     */
    private void checkAttributes(PersonContact entity) {
        Preconditions.checkNotNull(entity.getPhoneNumber());
        Preconditions.checkNotNull(entity.getAddress().getId());
        Preconditions.checkNotNull(entity.getFirstName());
        Preconditions.checkNotNull(entity.getSurname());
    }

    private class PersonContactRowMapper implements RowMapper<PersonContact> {
        @Override
        public PersonContact mapRow(ResultSet rs, int rowNum) throws SQLException {
            PersonContact contact = new PersonContact();
            contact.setId(rs.getLong(1));
            contact.setFirstName((rs.getString(3)));
            contact.setSurname(rs.getString(4));
            contact.setEmail(rs.getString(6));
            contact.setPhoneNumber(rs.getString(9));
            contact.setAddress(namedParamTemplateAddressRepository.findOne(rs.getLong(10)));
            return contact;
        }
    }
}
