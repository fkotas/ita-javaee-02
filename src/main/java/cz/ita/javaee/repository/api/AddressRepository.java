package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.Address;

public interface AddressRepository extends EntityRepository<Address> {
}
