package cz.ita.javaee.repository.api;

import com.google.common.base.Preconditions;
import cz.ita.javaee.model.CompanyContact;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;

/**
 * Created by Franta on 31.3.2017.
 */
@Repository("jdbcTemplateCompanyContactRepository")
@Profile("jdbcTemplate")
public class JdbcTemplateCompanyContactRepository implements CompanyContactRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private JdbcTemplateAddressRepository jdbcTemplateAddressRepository;

    private final CompanyContactRowMapper ROW_MAPPER = new CompanyContactRowMapper();

    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcTemplateCompanyContactRepository.class);

    /**
     * Find entity by id.
     * @param id identifier number of entity T
     * @return founded entity T
     */
    @Override
    public CompanyContact findOne(long id) {
        CompanyContact contact = jdbcTemplate.queryForObject("select * from contact where id = ? and type = 'COMPANY'", ROW_MAPPER, id);
        LOGGER.debug("Vraceno: "+contact.toString());
        return contact;
    }

    /**
     * Find entities.
     * @param offset is first record
     * @param limit is last record
     * @param orderBy is sort column name
     * @return list of founded entities
     */
    @Override
    public List<CompanyContact> find(int offset, int limit, final String orderBy) {
        List<CompanyContact> result = jdbcTemplate.query("select * from contact where type = 'COMPANY' order by "+orderBy+" limit ? offset ?", ROW_MAPPER, limit, offset);
        for(CompanyContact contact : result) {
            LOGGER.debug("Vraceno: "+contact.toString());
        }
        return result;
    }

    public List<CompanyContact> findByIds(String sql) {
        return jdbcTemplate.query(sql, ROW_MAPPER);
    }

    /**
     * Delete entity.
     * @param id identifier number of entity T
     * @return number of deleted records
     */
    @Override
    public int delete(long id) {
        int rowsDeleted = jdbcTemplate.update("delete from contact where id = ? and type = 'COMPANY'", id);
        LOGGER.debug("Smazano zaznamu: "+rowsDeleted);
        return rowsDeleted;
    }

    /**
     * @param entity entity with updated field identified by id
     * @return updated entity from repository
     *
     */
    @Override
    public CompanyContact update(final CompanyContact entity) {
        checkAttributes(entity);
        jdbcTemplate.update("update contact set company_name = ?, company_id = ?, vat_id = ?, email = ?, phone_number = ?, address_id = ? where id = ? and type='COMPANY'", entity.getCompanyName(), entity.getCompanyId(), entity.getVatId(), entity.getEmail(), entity.getPhoneNumber(), entity.getAddress().getId(), entity.getId());
        return findOne(entity.getId());
    }

    /**
     * @param entity entity class to store in repository
     * @return new persisted record with filled ID
     */
    @Override
    public CompanyContact create(final CompanyContact entity) {
        checkAttributes(entity);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement("insert into contact (type, company_name, company_id, vat_id, email, phone_number, address_id) values (?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1,"COMPANY");
                        ps.setString(2, entity.getCompanyName());
                        ps.setString(3, entity.getCompanyId());
                        ps.setString(4, entity.getVatId());
                        ps.setString(5, entity.getEmail());
                        ps.setString(6, entity.getPhoneNumber());
                        ps.setLong(7, entity.getAddress().getId());
                        return ps;
                    }
                }, keyHolder);
        entity.setId(keyHolder.getKey().longValue());
        return entity;
    }

    /**
     * Checks whether all mandatory fields of the CompanyContact are filled (not null).
     * @param entity CompanyContact on which to do the checks.
     *
     * @throws NullPointerException if any of the checks fails.
     */
    private void checkAttributes(CompanyContact entity) {
        Preconditions.checkNotNull(entity.getPhoneNumber());
        Preconditions.checkNotNull(entity.getAddress().getId());
        Preconditions.checkNotNull(entity.getCompanyName());
        Preconditions.checkNotNull(entity.getCompanyId());
    }

    private class CompanyContactRowMapper implements RowMapper<CompanyContact> {
        @Override
        public CompanyContact mapRow(ResultSet rs, int rowNum) throws SQLException {
            CompanyContact contact = new CompanyContact();
            contact.setId(rs.getLong(1));
            contact.setCompanyName((rs.getString(5)));
            contact.setEmail(rs.getString(6));
            contact.setCompanyId(rs.getString(7));
            contact.setVatId(rs.getString(8));
            contact.setPhoneNumber(rs.getString(9));
            contact.setAddress(jdbcTemplateAddressRepository.findOne(rs.getLong(10)));
            return contact;
        }
    }
}
