package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.Country;

public interface CountryRepository extends EntityRepository<Country> {
}
