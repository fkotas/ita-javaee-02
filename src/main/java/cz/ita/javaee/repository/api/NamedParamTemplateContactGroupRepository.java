package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.CompanyContact;
import cz.ita.javaee.model.Contact;
import cz.ita.javaee.model.ContactGroup;
import cz.ita.javaee.model.PersonContact;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Franta on 1.4.2017.
 */
@Repository
@Profile("namedParameterJdbcTemplate")
public class NamedParamTemplateContactGroupRepository implements ContactGroupRepository {

    @Autowired
    private NamedParameterJdbcTemplate namedParamTemplate;

    @Autowired
    private NamedParamTemplatePersonContactRepository namedParamTemplatePersonContactRepository;

    @Autowired
    private NamedParamTemplateCompanyContactRepository namedParamTemplateCompanyContactRepository;

    private final ContactGroupRowMapper ROW_MAPPER = new ContactGroupRowMapper();

    private static final Logger LOGGER = LoggerFactory.getLogger(NamedParamTemplateContactGroupRepository.class);
    /**
     * Find entity by id.
     * @param id identifier number of entity T
     * @return founded entity T
     */
    @Override
    public ContactGroup findOne(long id) {
        String sql = "select * from contact_group where id = :id";
        SqlParameterSource params = new MapSqlParameterSource("id", id);
        ContactGroup contactGroup = namedParamTemplate.queryForObject(sql, params, ROW_MAPPER);
        contactGroup.setContacts(findContactsForContactGroup(id));
        return contactGroup;
    }

    /**
     * Find entities.
     * @param offset is first record
     * @param limit is last record
     * @param orderBy is sort column name
     * @return list of founded entities
     */
    @Override
    public List<ContactGroup> find(int offset, int limit, final String orderBy) {
        String sql = "select * from contact_group order by "+orderBy+" limit :limit offset :offset";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("limit", limit);
        params.addValue("offset", offset);
        List<ContactGroup> result = namedParamTemplate.query(sql, params, ROW_MAPPER);
        for(ContactGroup contactGroup : result) {
            contactGroup.setContacts(findContactsForContactGroup(contactGroup.getId()));
        }
        return result;
    }
    /**
     * Finds all contacts for a ContactGroup.
     * @param contactGroupId id of ContactGroup we're findind contacts for
     * @return Set containing contacts for the ContactGroup.
     */
    private Set<Contact> findContactsForContactGroup(long contactGroupId) {
        //Nejprve vyhledame ids kontaktu patricich teto skupine kontaktu
        String contactIdsQuery = "select contact_id from contact_group_contact where contact_group_id = :id";
        SqlParameterSource params = new MapSqlParameterSource("id", contactGroupId);
        List<Long> contactIds = namedParamTemplate.queryForList(contactIdsQuery, params, Long.class);
        if(CollectionUtils.isEmpty(contactIds)) return new HashSet<Contact>();

        //musime udelat 2 dotazy, jeden pro kontakty typu PERSON a druhy pro typ COMPANY
        StringBuilder sql = new StringBuilder("select * from contact where type = 'PERSON' and id in (");
        for(Long contactId : contactIds) {
            sql.append(contactId);
            sql.append(",");
        }
        //smazeme posledni carku v sql dotazu a vlozime uzavirajici zavorku bloku in
        sql.deleteCharAt(sql.length()-1);
        sql.append(")");
        //vyhledani pro typ PERSON
        List<PersonContact> personContacts = namedParamTemplatePersonContactRepository.findByIds(sql.toString());
        //vyhledani pro typ COMPANY
        sql.replace(36,42,"COMPANY");
        List<CompanyContact> companyContacts = namedParamTemplateCompanyContactRepository.findByIds(sql.toString());

        //slouceni a vraceni vysledku obou dotazu
        Set<Contact> result = new HashSet<>();
        result.addAll(personContacts);
        result.addAll(companyContacts);
        return result;
    }

    /**
     * Delete entity.
     * @param id identifier number of entity T
     * @return number of deleted records
     */
    @Override
    public int delete(long id) {
        String sql = "delete from contact_group_contact where contact_group_id = :id";
        SqlParameterSource params = new MapSqlParameterSource("id", id);
        namedParamTemplate.update(sql, params);
        return namedParamTemplate.update("delete from contact_group where id = :id", params);
    }

    /**
     * @param entity entity with updated field identified by id
     * @return updated entity from repository
     */
    @Override
    public ContactGroup update(final ContactGroup entity) {
        String sqlUpdate = "update contact_group set name = :name where id = :id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", entity.getName());
        params.addValue("id", entity.getId());
        namedParamTemplate.update(sqlUpdate, params);

        Set<Contact> currentContacts = entity.getContacts();

        //smazeme zaznamy, co v contact_group_contact prebyvaji
        Set<Contact> oldContacts = findContactsForContactGroup(entity.getId());
        for(Contact contact : oldContacts) {
            if(!currentContacts.contains(contact)) {
                String sqlDelete = "delete from contact_group_contact where contact_id = :contactId and contact_group_id = :contactGroupId";
                params = new MapSqlParameterSource();
                params.addValue("contactId", contact.getId());
                params.addValue("contactGroupId", entity.getId());
                namedParamTemplate.update(sqlDelete, params);
            }
        }

        //pridame zaznamy, co v contact_group_contact chybi
        for(Contact contact : currentContacts) {
            String sql = "select count(*) from contact_group_contact where contact_group_id = :contactGroupId and contact_id = :contactId";
            params = new MapSqlParameterSource();
            params.addValue("contactId", contact.getId());
            params.addValue("contactGroupId", entity.getId());
            Integer count = namedParamTemplate.queryForObject(sql, params, Integer.class);
            if(count<1) {
                LOGGER.debug("Zaznam nebyl v tabulce, pridan zaznam s contactId "+contact.getId()+" a contact_group_id "+entity.getId());
                String sqlUpdateContacts = "insert into contact_group_contact (contact_group_id,contact_id) values (:contactGroupId, :contactId)";
                namedParamTemplate.update(sqlUpdateContacts, params);
            }
        }

        return findOne(entity.getId());
    }

    /**
     * @param entity entity class to store in repository
     * @return new persisted record with filled ID
     */
    @Override
    public ContactGroup create(final ContactGroup entity) {
        //pridani saznamu do tabulky contact_group
        String sql = "insert into contact_group (name) values (:name)";
        SqlParameterSource params = new MapSqlParameterSource("name", entity.getName());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParamTemplate.update(sql, params, keyHolder);
        entity.setId(keyHolder.getKey().longValue());

        //pridani vsech zaznamu do tabulky contact_group_contact
        for(Contact contact : entity.getContacts()) {
            String sqlUpdate = "insert into contact_group_contact (contact_group_id,contact_id) values (:contactGroupId, :contactId)";
            MapSqlParameterSource updateParams = new MapSqlParameterSource();
            updateParams.addValue("contactId", contact.getId());
            updateParams.addValue("contactGroupId", entity.getId());
            namedParamTemplate.update(sqlUpdate, updateParams);
        }

        return entity;
    }

    private class ContactGroupRowMapper implements RowMapper<ContactGroup> {
        @Override
        public ContactGroup mapRow(ResultSet rs, int rowNum) throws SQLException {
            ContactGroup contactGroup = new ContactGroup();
            contactGroup.setId(rs.getLong(1));
            contactGroup.setName(rs.getString(2));
            return contactGroup;
        }
    }
}
