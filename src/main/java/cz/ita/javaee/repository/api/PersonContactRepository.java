package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.PersonContact;

public interface PersonContactRepository extends EntityRepository<PersonContact> {
}
