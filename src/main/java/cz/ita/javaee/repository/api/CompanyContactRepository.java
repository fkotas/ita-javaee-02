package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.CompanyContact;

public interface CompanyContactRepository extends EntityRepository<CompanyContact> {
}
