package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Franta on 1.4.2017.
 */
@Repository
@Profile("namedParameterJdbcTemplate")
public class NamedParamTemplateCountryRepository implements CountryRepository {

    @Autowired
    private NamedParameterJdbcTemplate namedParamTemplate;

    private final CountryRowMapper ROW_MAPPER = new CountryRowMapper();

    private static final Logger LOGGER = LoggerFactory.getLogger(NamedParamTemplateCountryRepository.class);

    /**
     * Find entity by id.
     * @param id identifier number of entity T
     * @return founded entity T
     */
    @Override
    public Country findOne(long id) {
        String sql = "select * from country where id = :id";
        SqlParameterSource params = new MapSqlParameterSource("id", id);
        Country country = namedParamTemplate.queryForObject(sql, params, ROW_MAPPER);
        LOGGER.debug("Vraceno z db: "+country.toString());
        return country;
    }

    /**
     * Find entities.
     * @param offset is first record
     * @param limit is last record
     * @param orderBy is sort column name
     * @return list of founded entities
     */
    @Override
    public List<Country> find(int offset, int limit, final String orderBy) {
        String sql = "select * from country order by "+orderBy+" limit :limit offset :offset";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("limit", limit);
        params.addValue("offset", offset);
        return namedParamTemplate.query(sql, params, ROW_MAPPER);
    }

    /**
     * Delete entity.
     * @param id identifier number of entity T
     * @return number of deleted records
     */
    @Override
    public int delete(long id) {
        String sql = "delete from country where id = :id";
        SqlParameterSource params = new MapSqlParameterSource("id", id);
        return namedParamTemplate.update(sql, params);
    }

    /**
     * @param entity entity with updated field identified by id
     * @return updated entity from repository
     */
    @Override
    public Country update(final Country entity) {
        String sql = "update country set name = :name, iso_code = :isoCode where id = :id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", entity.getName());
        params.addValue("isoCode", entity.getIsoCode());
        params.addValue("id", entity.getId());
        namedParamTemplate.update(sql, params);
        return findOne(entity.getId());
    }

    /**
     * @param entity entity class to store in repository
     * @return new persisted record with filled ID
     */
    @Override
    public Country create(final Country entity) {
        String sql = "insert into country (name, iso_code) values (:name,:isoCode)";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", entity.getName());
        params.addValue("isoCode", entity.getIsoCode());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParamTemplate.update(sql, params, keyHolder);
        entity.setId(keyHolder.getKey().longValue());
        return entity;
    }

    private class CountryRowMapper implements RowMapper<Country> {
        @Override
        public Country mapRow(ResultSet rs, int rowNum) throws SQLException {
            Country country = new Country();
            country.setId(rs.getLong(1));
            country.setName(rs.getString(2));
            country.setIsoCode(rs.getString(3));
            return country;
        }
    }
}
