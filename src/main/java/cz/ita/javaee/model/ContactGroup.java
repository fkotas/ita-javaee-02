package cz.ita.javaee.model;

import java.util.Set;

public class ContactGroup extends AbstractDO {
    private String name;
    private Set<Contact> contacts;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        return "ContactGroup{" +
                "name='" + name + '\'' +
                ", contacts=" + contacts +
                '}';
    }
}
