package cz.ita.javaee.model;

import static cz.ita.javaee.model.Contact.ContactType.*;

public class PersonContact extends Contact {
    public String firstName;
    public String surname;

    public PersonContact(String firstName, String surname, String phoneNumber, String email, Address address) {
        super(PERSON, phoneNumber, email, address);
        this.firstName = firstName;
        this.surname = surname;
    }

    public PersonContact() {
        super(PERSON);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "PersonContact{" +
                "firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
