package cz.ita.javaee.model;

import static cz.ita.javaee.model.Contact.ContactType.COMPANY;

public class CompanyContact extends Contact {
    private String companyName;
    private String companyId;
    private String vatId;

    public CompanyContact() {
        this(null, null, null, null, null, null);
    }

    public CompanyContact(String companyName, String companyId,
            String vatId, String phoneNumber, String email, Address address) {
        super(COMPANY, phoneNumber, email, address);
        this.companyName = companyName;
        this.companyId = companyId;
        this.vatId = vatId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getVatId() {
        return vatId;
    }

    public void setVatId(String vatId) {
        this.vatId = vatId;
    }

    @Override
    public String toString() {
        return "CompanyContact{" +
                "companyName='" + companyName + '\'' +
                ", companyId='" + companyId + '\'' +
                ", vatId='" + vatId + '\'' +
                '}';
    }
}
