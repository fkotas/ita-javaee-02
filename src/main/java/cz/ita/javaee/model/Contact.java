package cz.ita.javaee.model;

public abstract class Contact extends AbstractDO {
    public enum ContactType {
        PERSON, COMPANY;
    }
    private ContactType type;
    private String phoneNumber;
    private String email;
    private Address address;

    public Contact(ContactType type, String phoneNumber, String email, Address address) {
        this.type = type;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
    }

    public Contact(final ContactType type) {
        this.type = type;
    }

    public ContactType getType() {
        return type;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "type=" + type +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", address=" + address +
                '}';
    }
}
