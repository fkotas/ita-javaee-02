CREATE TABLE IF NOT EXISTS COUNTRY (
  id            BIGSERIAL PRIMARY KEY,
  name          VARCHAR(50) NOT NULL,
  iso_code      VARCHAR(3) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS ADDRESS (
    id            BIGSERIAL PRIMARY KEY,
    street         VARCHAR(30) NOT NULL,
    street_number  VARCHAR(10) NOT NULL,
    city          VARCHAR(30) NOT NULL,
    zip_code      VARCHAR(6) NOT NULL,
    country_id      BIGINT NOT NULL REFERENCES COUNTRY (id)
);

CREATE TABLE IF NOT EXISTS CONTACT (
    id           BIGSERIAL PRIMARY KEY,
    type          VARCHAR(10) NOT NULL,
    firstname     VARCHAR(100),
    surname       VARCHAR(100),
    company_name  VARCHAR(100),
    email         VARCHAR(100),
    company_id    VARCHAR(10) UNIQUE,
    vat_id        VARCHAR(12) UNIQUE,
    phone_number   VARCHAR(20) NOT NULL,
    address_id    BIGINT NOT NULL REFERENCES ADDRESS (id)
);

-- CREATE TABLE IF NOT EXISTS CONTACT (
--     id            NUMERIC(20,0) PRIMARY KEY,
--     email         VARCHAR(100),
--     phone_number   VARCHAR(20) NOT NULL,
--     address_id    BIGINT NOT NULL REFERENCES ADDRESS (id)
-- );

-- CREATE TABLE IF NOT EXISTS PERSON_CONTACT (
--     id            BIGSERIAL PRIMARY KEY,
--     firstname     VARCHAR(100) NOT NULL,
--     surname       VARCHAR(100) NOT NULL,
--     contact_id    BIGINT NOT NULL REFERENCES CONTACT (id)
-- );
--
-- CREATE TABLE IF NOT EXISTS COMPANY_CONTACT (
--     id            BIGSERIAL PRIMARY KEY,
--     name          VARCHAR(100) NOT NULL,
--     company_id    VARCHAR(10) NOT NULL UNIQUE,
--     vat_id        VARCHAR(12) UNIQUE,
--     contact_id    BIGINT NOT NULL REFERENCES CONTACT (id)
-- );

CREATE TABLE IF NOT EXISTS CONTACT_GROUP (
    id            BIGSERIAL PRIMARY KEY,
    name          VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS CONTACT_GROUP_CONTACT (
    contact_id        BIGINT NOT NULL REFERENCES CONTACT (id),
    contact_group_id  BIGINT NOT NULL REFERENCES CONTACT_GROUP (id)
);
